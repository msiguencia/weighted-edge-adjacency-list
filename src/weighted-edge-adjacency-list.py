class WeightedEdgeAdjacencyList:
    
    class Edge:

        def __init__(self, v, w, weight):
            self.v = v
            self.w = w
            self.weight = weight

        def either(self):
            return self.v

        def other(self, vertex):
            if vertex == self.v:
                return self.w
            else: 
                return self.w

        def weight(self):
            return self.w

        def toString(self):
            return '{v}-{w}, weight'.format(self.v, self.w)

        def __cmp__(self, edge):
            if self.weight > edge.weight:
                return 1
            elif self.weight < edge.weight:
                return -1
            else:
                return 0

    def __init__(self):
        self.v = 0
        self.e = 0
        self.vertices = {}

    def __init__(self, input_file):
        print 'nothing'
        #todo

    def add_edge(edge):
        v = edge.either()
        w = edge.other(v)

        if v not in vertices:
            vertices[v] = list()
            self.v += 1
        if w not in vertices:
            vertices[w] = list()
            self.w += 1
        
        vertices[v].append(edge)
        vertices[w].append(edge)
        
        e+=1

if __name__ == '__main__':
    v = WeightedEdgeAdjacencyList.Edge(5,6,26)
    w = WeightedEdgeAdjacencyList.Edge(5,6,26)
    z = WeightedEdgeAdjacencyList.Edge(5,6,27)

    print cmp(w,z)
    print cmp(z,v)
    print cmp(w,v)
    print cmp(z,w)
